VERSION="1.2"
docker login
docker build -t multilanguage-jupyter-lab .
docker tag multilanguage-jupyter-lab nicolalandro/multilanguage-jupyter-lab:$VERSION
docker push nicolalandro/multilanguage-jupyter-lab:$VERSION

