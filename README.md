# Jupyter LAB
This project contain a dockerfile and a docker-compose for running jupyter lab with different language and latex.
![jupyter lab](launcher.png?raw=true "Image with jupyter lab and his launcher")

```
docker-compose up
firefox http://localhost:8888
```
It is also pushed to [DockerHub](https://hub.docker.com/r/nicolalandro/multilanguage-jupyter-lab).

# List of kernel
* Python3
* [SoS (Plugin for multi-lenguage into one Notebook)](https://vatlab.github.io/sos-docs/)
* [Coq (Proof assistant)](https://github.com/EugeneLoy/coq_jupyter)
* [Java](https://github.com/SpencerPark/IJava)
* [Scala](https://almond.sh/)
* [Ruby](https://github.com/sciruby/iruby)
* [Javascript](https://github.com/n-riesco/ijavascript)

