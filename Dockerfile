FROM ubuntu

# Install coq e python
RUN apt-get update && apt-get install -y coq coqide python3-dev python3
RUN apt-get update && apt-get install -y python3-pip

# Install java
RUN apt-get update && apt-get install -y openjdk-11-jdk

# Install scala
RUN apt-get update && apt-get install -y wget
RUN wget https://downloads.lightbend.com/scala/2.11.0/scala-2.11.0.tgz && tar -xvzf scala-2.11.0.tgz && rm scala-2.11.0.tgz
ENV SCALA_HOME=/scala-2.11.0
ENV export PATH=$PATH:$SCALA_HOME/bin:$PATH

# Install Ruby
RUN apt-get update && apt-get install -y libtool libffi-dev ruby ruby-dev make
RUN apt-get update && apt-get install -y libzmq3-dev libczmq-dev

# Install Nodejs
RUN apt-get update && apt-get install -y nodejs npm

# Install jupyter and python dependency
WORKDIR /server
ADD requirements.txt /server/requirements.txt
RUN pip3 install -r requirements.txt

# Configure jupyter plugin for install extension
RUN jupyter contrib nbextension install --user
RUN jupyter nbextensions_configurator enable --user

# Configure coq (proof assistant)
RUN python3 -m coq_jupyter.install

# Configure sos (for multi lenguage into a notebook)
RUN python3 -m sos_notebook.install

# Configure Java
ADD ijava-1.3.0 ./ijava-1.3.0
RUN ls
RUN cd ijava-1.3.0 && python3 install.py --sys-prefix

# Configure Scala
ADD almond .
RUN ./almond --install

# Configure Ruby
RUN gem install cztop
RUN gem install iruby --pre
RUN iruby register --force

# Configure javascript
RUN npm install -g ijavascript
RUN ijsinstall

# jupyter lab
RUN pip3 install jupyterlab

# Draw.io plugin
RUN jupyter labextension install jupyterlab-drawio

# latex plugin
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y texlive-latex-extra texlive-science texlive-lang-italian curl wget
RUN apt-get update && apt-get install -y texlive-xetex

RUN pip3 install jupyterlab_latex
RUN jupyter labextension install @jupyterlab/latex
RUN jupyter serverextension enable --sys-prefix jupyterlab_latex

# ZSH
RUN apt-get update && apt-get install -y zsh
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" || true

# For SoS (notebook multilenguage)
RUN pip3 install --upgrade jupyterlab
RUN jupyter labextension install transient-display-data
RUN jupyter labextension install jupyterlab-sos

# Github 
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager
# RUN jupyter labextension install @jupyterlab/git

CMD jupyter lab --ip=0.0.0.0 --port=8888 --allow-root --NotebookApp.token='' --no-browser
